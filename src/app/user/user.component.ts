import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Component, OnInit } from '@angular/core';
import { User } from '../services/users/models';

import { UserService } from '../services/users/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user: User;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.userService.getUserByID(params.id)
        .subscribe((user: any)  => this.user = user);
    });
  }

  goBack(): void {
    this.location.back();
  }
}
