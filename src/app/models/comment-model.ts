// model for obj type of comment

export class CommentModel {

  id: number;
  author: number;
  value: string;
  date?: Date;
}
