import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_URL_USERS } from '../../consts';

@Injectable()
export class UserService {
  private baseUrl = API_URL_USERS;

  constructor(
    private http: HttpClient,
  ) { }

  getUserByID(id: number) {
    const url = `${this.baseUrl}/${id}`;
    return this.http.get(url);
  }
}
