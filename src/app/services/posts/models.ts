// import { Models } from '../users/models';

export class Post {
  id: number;
  title: string;
  body: string;
  userId: number;
  comments?: Comment[];

  constructor(title: string, body: string, userId: number) {
    this.title = title;
    this.body = body;
    this.userId = userId;
  }
}
export class Comment {
  body: string = null;
  email: string = null;
  id: number = null;
  name: string = null;
  postId: number = null;
}
