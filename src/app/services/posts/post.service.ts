import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { API_URL_POSTS } from '../../consts';

import { Post } from './models';
import { Comment } from './models';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root'})

export class PostService {
  private baseUrl = API_URL_POSTS;
  constructor(
    private http: HttpClient,
  ) {
  }

  /** GET posts from api url with passing optional? params */
  getPosts(page?: number, limit?: number) {
    const data = {
      _start: (page * limit).toString(),
      _limit: limit.toString()
    };
    return this.http.get(this.baseUrl, {
      params: data
    });
  }

  getPostByID(id: number): Observable<Post> {
    const url = `${this.baseUrl}/${id}`;
    return this.http.get<Post>(url);
  }

  getComments(id: number) {
    const url = `${this.baseUrl}/${id}/comments`;
    return this.http.get<Comment[]>(url);
  }

  getPost(id: number) {
    const url = `${this.baseUrl}/${id}`;
    return this.http.get(url);
  }

  addPost(post: Post) {
    return this.http.post(this.baseUrl, post, httpOptions);
  }
}
