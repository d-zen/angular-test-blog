import { NgModule } from '@angular/core';
import { RouterModule , Routes } from '@angular/router';

import { PostsComponent } from 'src/app/posts/posts.component';
import { SinglePostComponent } from 'src/app/posts/single-post/single-post.component';
import { CreatePostComponent } from 'src/app/posts/create-post/create-post.component';
import { UserComponent } from 'src/app/user/user.component';

const routes: Routes = [
  { path: '', redirectTo: '/posts', pathMatch: 'full' },
  { path: 'create-post', component: CreatePostComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'posts/:id', component: SinglePostComponent },
  { path: 'users/:id', component: UserComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})

export class AppRoutingModule {}
