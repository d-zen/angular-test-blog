import { Component, OnInit } from '@angular/core';

import { Post, Comment } from '../services/posts/models';
import { User } from '../services/users/models';

import { PostService } from '../services/posts/post.service';
import { UserService } from '../services/users/user.service';
import { ModalService } from '../modals/modal.service';
import { AuthorModalComponent } from '../modals/author-modal/author-modal.component';
import { CommentsModalComponent } from '../modals/comments-modal/comments-modal.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  public currentPage = 0;
  public limit = 10;
  public pages = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

  public posts: Post[] = [];
  public comments: string[] = [];

  public singlePost: Post;
  public author: User;

  constructor(
    private route: ActivatedRoute,
    private modalService: ModalService,
    private postService: PostService,
    private router: Router,
    private userService: UserService,
  ) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.currentPage = params.page ? parseInt(params.page, 10) : 0;
      this.limit = params.limit ? parseInt(params.limit, 10) : 10;
      this.getPosts();
    });
  }

  getPosts(): void {
    this.postService.getPosts(this.currentPage, this.limit).subscribe((posts: any) => this.posts = posts);
  }

  getPostComments(post: Post) {
    this.postService.getComments(post.id).subscribe((res: Comment[]) => {
      this.singlePost.comments = res;
    });
  }

  goToPage(page: number) {
    this.currentPage = page;
    this.router.navigate(['/posts'],
      {queryParams: {page: this.currentPage, limit: this.limit}, relativeTo: this.route});
  }
  nextPage() {
    this.currentPage += 1;
    this.goToPage(this.currentPage);
  }
  previousPage() {
    this.currentPage -= 1;
    this.goToPage(this.currentPage);
  }

  add(title: string, body: string): void {
    title = title.trim();
    if (!title) {
      return;
    }
    this.postService.addPost({title, body} as Post)
      .subscribe((post: any) => {
        this.posts.push(post);
      });
  }

  goToPost(id: number) {
    this.router.navigate(['/posts', id]);
  }

  showPost(post: Post) {
    this.singlePost = post;
    this.getPostComments(post);
    this.modalService.open(CommentsModalComponent.ID, this.singlePost);
  }

  showAuthor(id: number) {
    this.userService.getUserByID(id).subscribe((res: any) => {
      this.author = res;
      this.modalService.open(AuthorModalComponent.ID, this.author);
    });
  }
}
