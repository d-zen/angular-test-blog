import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Component, OnInit} from '@angular/core';
import { Post , Comment } from '../../services/posts/models';
import { User } from '../../services/users/models';

import { PostService } from '../../services/posts/post.service';
import { UserService } from '../../services/users/user.service';

@Component({
  selector: 'app-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.css']
})

export class SinglePostComponent implements OnInit {

  post: Post;
  comments: Comment[];
  author: User;

  constructor(
    private route: ActivatedRoute,
    private postService: PostService,
    private usersService: UserService,
    private location: Location
) {}

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.postService.getPostByID(params.id).subscribe( post => {
        this.post = post;
        this.usersService.getUserByID(this.post.userId).subscribe( (author: any) => {
          this.author = author;
        });
      });
      this.postService.getComments(params.id).subscribe(comments => {
        this.comments = comments;
      });
    });
  }
  goBack(): void {
    this.location.back();
  }
}
