import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { PostService } from '../../services/posts/post.service';
import { Post } from '../../services/posts/models';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent {

  constructor(
    private location: Location,
    private postService: PostService
  ) {
  }

  goBack(): void {
    this.location.back();
  }

  save(title: string, body: string): void {
    title = title.trim();
    const userId = 1;
    this.postService.addPost({title, body, userId} as Post)
      .subscribe(created => {
        alert('userId: ' + userId + ', title: ' + title + ', postBody: ' + body + 'has been created.');
        this.location.back();
      });
  }
}
