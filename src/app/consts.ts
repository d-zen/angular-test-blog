// api url used for receive data

export const API_URL = 'https://jsonplaceholder.typicode.com';
export const API_URL_POSTS = 'https://jsonplaceholder.typicode.com/posts';
export const API_URL_USERS = 'https://jsonplaceholder.typicode.com/users';
