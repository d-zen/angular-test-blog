import { Component, ElementRef, OnInit } from '@angular/core';
import { ModalService } from '../modal.service';
import { AbstractModal } from '../modal.component';
import { Comment, Post } from '../../services/posts/models';
import { PostService } from '../../services/posts/post.service';

@Component({
  selector: 'app-comments-modal',
  templateUrl: './comments-modal.component.html',
  styleUrls: [
    '../modal.component.scss',
    './comments-modal.component.scss'
  ]
})
export class CommentsModalComponent extends AbstractModal implements OnInit {

  public static readonly ID = 'comments-modal';
  protected id = CommentsModalComponent.ID;
  public post: any;
  comments: Comment[] = [];

  constructor(
    protected postService: PostService,
    protected modalService: ModalService,
    protected el: ElementRef,
  ) {
    super(modalService, el);
  }

  open(post: Post): void {
    this.post = post;
    this.getPostComments(post.id);
    this.isHidden = false;
    this.element.show();
  }

  getPostComments(id: number): void {
    this.postService.getComments(id).subscribe(res => this.comments = res);
  }
}
