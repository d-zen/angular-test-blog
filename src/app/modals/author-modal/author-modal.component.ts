import { Component, OnInit, ElementRef } from '@angular/core';
import { ModalService } from '../modal.service';
import { AbstractModal } from '../modal.component';

@Component({
  selector: 'app-author-modal',
  templateUrl: './author-modal.component.html',
  styleUrls: [
    '../modal.component.scss',
    './author-modal.component.scss',
  ]
})
export class AuthorModalComponent extends AbstractModal  {
  public static readonly ID = 'author-modal';
  protected id = AuthorModalComponent.ID;
  public author: any;

  constructor(
    protected modalService: ModalService,
    protected el: ElementRef,
  ) {
    super(modalService, el);
  }

  open(author: any): void {
    this.author = author;
    this.isHidden = false;
    this.element.show();
  }


}
