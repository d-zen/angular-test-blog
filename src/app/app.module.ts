import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';


// services
import { UserService } from './services/users/user.service';
import { PostService } from './services/posts/post.service';
import { ModalService } from './modals/modal.service';

// components
import { AppComponent } from './app.component';
import { PostsComponent } from './posts/posts.component';
import { CommentsModalComponent } from './modals/comments-modal/comments-modal.component';
import { AuthorModalComponent } from './modals/author-modal/author-modal.component';

// modal
import { SinglePostComponent } from './posts/single-post/single-post.component';
import { CreatePostComponent } from './posts/create-post/create-post.component';
import { UserComponent } from './user/user.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  declarations: [
    AppComponent,
    PostsComponent,
    CommentsModalComponent,
    AuthorModalComponent,
    SinglePostComponent,
    CreatePostComponent,
    UserComponent,
  ],
  providers: [
    PostService,
    UserService,
    ModalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
